<?php

namespace Database\Factories;

use App\Models\Listing;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Listing::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $item_ids = config('database.default') == 'sqlite'
            # sqlite does not support custom autoincrement init
            ? range(1,25)
            # mysql, pgsql will initialize autoincrement to 10101
            : range(10101,10125)
        ;

        return [
            'item_id' => collect($item_ids)->random(),
            'ask_price' => $this->faker->numberBetween(1.00, 599.99),
            'offers' => [],
        ];
    }
}
