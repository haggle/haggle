<?php

namespace Database\Factories;

use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    protected $items = [
        'Amp',
        'Subwoofers',
        'Bicycle',
        'Motorcycle',
        'Couch',
        'Mattress',
        'Hammock',
        'Kayak',
        'Classic car',
        'Flatscreen',
        'Book',
        'Legos',
        'Heavy bag',
        'Wok',
        'Slow cooker',
    ];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'label' => collect($this->items)->random(), #$this->faker->word,
            'description' => $this->faker->realText,
            'images' => [],
        ];
    }
}
