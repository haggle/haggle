<div class="flex justify-center pt-4 sm:justify-start sm:pt-0">
    <h3>Recently listed</h3>
    <ul class="flex justify-start">
    @foreach( $listings as $listing )
        <li>
            <form action="">
                <a style="display:inline-block;width:168px" title="{{ $listing->description }}" href="{{ $listing->route }}">
                  {{ $listing->label }} - $ {{ $listing->ask_price }}
                </a>

                <button>Bid</button>
                <button>Message</button>
                {{-- <button>Quick claim</button> --}}
            </form>
        </li>
    @endforeach
    </ul>
</div>

<style>
    ul { list-style: none; }
</style>
