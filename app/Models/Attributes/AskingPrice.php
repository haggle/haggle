<?php

namespace App\Models\Attributes;

trait AskingPrice
{
    public function getAskPriceAttribute()
    {
        return $this->attributes['ask_price'] / 100;
    }

    public function setAskPriceAttribute($value)
    {
        $this->attributes['ask_price'] = $value * 100;
    }
}
