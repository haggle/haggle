<?php

namespace App\Models;

use App\Models\
{
    Base\HaggleModel as Model,
    Attributes\AskingPrice
};

class Listing extends Model
{
    use AskingPrice;

    protected $casts = [
        'offers' => 'json',
    ];

    public $with = ['item'];

    public function item()
    {
        return $this->belongsTo( Item::class );
    }

    public function getRouteAttribute()
    {
        return route('listings.show', $this);
    }

    public function getLabelAttribute()
    {
        // pass-thru
        return $this->item->label;
    }

    public function getDescriptionAttribute()
    {
        // pass-thru
        return $this->item->description;
    }

    public function getImagesAttribute()
    {
        // pass-thru
        return $this->item->images();
    }
}
