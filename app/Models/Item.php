<?php

namespace App\Models;

use App\Models\Base\HaggleModel as Model;

class Item extends Model
{
    protected $casts = [
        'images' => 'array',
    ];
}
