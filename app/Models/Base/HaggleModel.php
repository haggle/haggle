<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\{
    Model,
    Factories\HasFactory
};

class HaggleModel extends Model
{
    use HasFactory;
}
